import { useEffect, useState } from "react"

const Imagem = ()=>{
    const [imagem, setImagem] = useState({})
    const [param, setParam] = useState()
    const getImage = () => {
        return fetch(`http://localhost:8080/api/imagens/${param}`)
        .then(response => response.json())
    }
    useEffect((param)=>{
        getImage(param).then((result)=>{
            setImagem(result)
        })
    },[param])
    return (
    <div>
        <p>Digite abaixo o id da imagem:</p>
        <input type='text' onInput={(e) => setParam(e.target.value)} placeholder="Digite o id aqui"/>
        <br /><br />
        <img alt="local da imagem" src={`data:${imagem.contentType};base64,${imagem.contentBytes}`} />
    </div>
    )
} 

export default Imagem
  