package br.com.lgovea.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import br.com.lgovea.domain.Imagem;
import br.com.lgovea.repository.ImagemRepository;

@Service
public class ImagemService {
	
	@Autowired
	ImagemRepository imagemRepository;
	
	public Imagem inserir(MultipartFile file) throws IOException {
		Imagem img = new Imagem();
		img.setContentBytes(file.getBytes());
		img.setContentType(file.getContentType());
		return imagemRepository.save(img);
	}
	
	public Imagem buscar(Long id) {
		return imagemRepository.findById(id).get();
	}
	
	
}
