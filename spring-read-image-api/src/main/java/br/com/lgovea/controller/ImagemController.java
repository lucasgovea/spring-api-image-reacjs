package br.com.lgovea.controller;

import java.io.IOException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.lgovea.domain.Imagem;
import br.com.lgovea.service.ImagemService;

@CrossOrigin("*")
@RequestMapping("/api/imagens")
@RestController
public class ImagemController {
	
	@Autowired
	ImagemService imagemService;
	
	@PostMapping
	public ResponseEntity<Imagem> inserir(@RequestPart MultipartFile file) throws IOException {
		return ResponseEntity.status(HttpStatus.OK).body(imagemService.inserir(file));
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Imagem> buscar(@PathVariable Long id) throws IOException {
		Optional<Imagem> imgOpt = imagemService.buscar(id);
		if(imgOpt.isPresent()) {
			return ResponseEntity.status(HttpStatus.OK).body(imgOpt.get());
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	}
	
}

