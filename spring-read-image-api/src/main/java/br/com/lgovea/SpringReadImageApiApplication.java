package br.com.lgovea;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringReadImageApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringReadImageApiApplication.class, args);
	}

}
