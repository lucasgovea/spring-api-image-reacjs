package br.com.lgovea.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.lgovea.domain.Imagem;

@Repository
public interface ImagemRepository extends JpaRepository<Imagem, Long> {
}
